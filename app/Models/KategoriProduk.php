<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KategoriProduk extends Model
{
    use HasFactory;
    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'kategori_produk';

    static function storeKategoriProduk($request)
    {
        KategoriProduk::create([
            'kp_nama' => $request->kp_nama,
            'kp_active'  => $request->kp_active
        ]);
    }
}
