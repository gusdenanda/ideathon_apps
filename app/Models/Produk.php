<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;
    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'produk';

    static function storeProduk($request)
    {
        Produk::create([
            'produk_nama' => $request->produk_nama,
            'produk_deskripsi'  => $request->produk_deskripsi,
            'produk_stok'  => $request->produk_stok,
            'produk_harga'  => $request->produk_harga,
            'produk_gambar'  => $request->produk_gambar,
            'store_id'  => $request->store_id,
            'produk_active'  => $request->produk_active
        ]);
    }
}
