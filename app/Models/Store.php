<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use HasFactory;
    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'store';

    static function storeStore($request)
    {
        PenandatanganSwab::create([
            'store_nama' => $request->store_nama,
            'store_deskripsi' => $request->store_deskripsi,
            'store_alamat' => $request->store_alamat,
            'store_email' => $request->store_email,
            'store_no_hp'  => $request->store_no_hp,
            'store_nama_pemilik' => $request->store_nama_pemilik,
            'store_logo' => $request->store_logo,
            'store_lat' => $request->store_lat,
            'store_lng'  => $request->store_lng
        ]);
    }
}
