<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class HomeController extends Controller
{
     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::check()) {
			if (Auth::user()->hasRole('admin')) {
                return redirect('/admin/master/user');
            } elseif (Auth::user()->hasRole('pelapor')) {
                return redirect('/pelapor/pengaduan');
            } elseif (Auth::user()->hasRole('departemen')) {
                return redirect('/departemen/dashboard');
            } elseif (Auth::user()->hasRole('pengawas')) {
                return redirect('/pengawas/dashboard');
            }
		}else{
            return view('pages.front.login');
        }
    }
}

