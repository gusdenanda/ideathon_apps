<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Produk;
use App\Models\Store;
use App\Models\KategoriProduk;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $store = Store::select('id', 'store_nama')
                            ->get();
        $kp = KategoriProduk::select('id', 'kp_nama')
                            ->where('kp_active', '1')
                            ->get();
        return view('pages.admin.produk',compact('store','kp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'logo'      => 'nullable|max:1024'
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }
        $logo = "";
        if ($request->logo != NULL) {
            $image = $request->file('logo');
            $logo = rand() . '.' . $image->getClientOriginalExtension();
            $image->move('repositories/public/images', $logo);
        }
        Produk::create([
            'produk_nama' => $request->produk_nama,
            'produk_deskripsi'  => $request->produk_deskripsi,
            'produk_stok'  => $request->produk_stok,
            'produk_harga'  => $request->produk_harga,
            'produk_gambar'  => $logo,
            'store_id'  => $request->store_id,
            'kp_id'  => $request->kp_id,
            'produk_active'  => $request->produk_active
        ]);
        
        return $this->status(1, 'Data berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Produk::where('id', $id)->first();
        return json_encode($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'logo'      => 'nullable|max:1024'
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }
        $logo = "";
        if ($request->logo != NULL) {
            $image = $request->file('logo');
            $logo = rand() . '.' . $image->getClientOriginalExtension();
            $image->move('repositories/public/images', $logo);
        }
        if ($request->ttd != NULL) {
            $image = $request->file('ttd');
            $ttd = rand() . '.' . $image->getClientOriginalExtension();
            $image->move('repositories/public/images', $ttd);
            Produk::where('id', $id)->update([
                'produk_nama' => $request->produk_nama,
                'produk_deskripsi'  => $request->produk_deskripsi,
                'produk_stok'  => $request->produk_stok,
                'produk_harga'  => $request->produk_harga,
                'produk_gambar'  => $logo,
                'store_id'  => $request->store_id,
                'kp_id'  => $request->kp_id,
                'produk_active'  => $request->produk_active
            ]);
        }else{
            Produk::where('id', $id)->update([
                'produk_nama' => $request->produk_nama,
                'produk_deskripsi'  => $request->produk_deskripsi,
                'produk_stok'  => $request->produk_stok,
                'produk_harga'  => $request->produk_harga,
                'store_id'  => $request->store_id,
                'kp_id'  => $request->kp_id,
                'produk_active'  => $request->produk_active
            ]);
        }

        return $this->status(1, 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Produk::where('id', $id)->delete();
        
        return $this->status(1, 'Data berhasil dihapus');
    }
    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }
    public function data()
    {
        $data = Produk::select(['id', 'produk_nama','produk_deskripsi','produk_harga', 'produk_stok','produk_active']);
        
        return DataTables::of($data)
            ->addColumn('aksi', function ($item) {
                return '<form action="produk/destroy/'. $item->id .'" class="text-center" method="POST"><div class="btn-group"><button type="button" class="btn btn-warning ubah" data-value="'. $item->id .'" data-nama="'. $item->produk_nama .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button><button type="button" class="btn btn-danger hapus" onclick="hapus('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button></div></form>';
            })
            ->addColumn('produk_active', function ($item) {
                if($item->produk_active=="1"){
                    $st = '<div disabled class="badge badge-md badge-success">Aktif</div>';
                }else{
                    $st = '<div disabled class="badge badge-md badge-danger">Tidak Aktif</div>';
                }
                return $st;
            })
            ->rawColumns(['aksi','produk_active'])
            ->removeColumn('id')
            ->make(true);
    }
}
