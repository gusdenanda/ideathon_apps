@if (request()->is('admin') || request()->is('admin/*'))
    <li class="navigation-header"><span>Analytics</span><i data-feather="more-horizontal"></i></li>
    {{-- Data Master--}}
    <li class="navigation-header"><span>Master</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.user.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin.master.user.index') }}"><i data-feather="user"></i><span class="menu-title text-truncate">User</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.store.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin.master.store.index') }}"><i data-feather="archive"></i><span class="menu-title text-truncate">Store</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.kategori-produk.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin.master.kategori-produk.index') }}"><i data-feather="box"></i><span class="menu-title text-truncate">Kategori Produk</span></a></li>
    {{-- Data Master--}}
    <li class="navigation-header"><span>Produk</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.produk.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin.master.produk.index') }}"><i data-feather="briefcase"></i><span class="menu-title text-truncate">Manajemen Produk</span></a></li>
    
@endif