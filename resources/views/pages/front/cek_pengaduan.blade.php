@extends('front.layouts.app')
@section('title', 'Cek Pengaduan')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
        <section id="pengaduan" class="contact section-bg" style="padding: 120px 0 60px 0">
            <div class="container" data-aos="fade-up">
    
            <div class="section-title offset-1">
                <h2>Pengaduan Online</h2>
                <p style="text-transform: initial;">Rs. Bhayangkara</p>
            </div>
                <div class="row">
                    <div class="col-lg-10 offset-1">
                        <form action="{{ route('searchpengaduan') }}" method="post" role="form" class="php-email-form" enctype="multipart/form-data">
                            @csrf
                            <div class="complaint-form-box">
                                <div class="select-complaint">Cek Status Laporan Anda</div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="ticket" id="ticket" placeholder="Masukkan Nomer Ticket Pengaduan Anda" required/>
                                <div class="validate"></div>
                            </div>
                            <div class="text-center"><button type="submit">Kirim</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section><!-- End Contact Section -->
@endsection
@section('js')
    <script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
			// Tambah/Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					$('#modal').modal('hide');
					formExecuted(response);
				});
			});
		});

        function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				location.href ='{{url('/home')}}';
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});
		}
	</script>
@endsection
