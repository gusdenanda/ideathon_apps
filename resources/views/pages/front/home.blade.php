@extends('front.layouts.app')
@section('title', 'Home')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
    @if ($message["status"]==1)
        <section id="pengaduan" class="contact section-bg" style="padding: 120px 0 60px 0">
            <div class="container" data-aos="fade-up">
    
            <div class="section-title offset-1">
                <h2>Pengaduan Online</h2>
                <p style="text-transform: initial;">Rs. Bhayangkara</p>
            </div>
                <div class="row">
                    <div class="col-lg-10 offset-1">
                        <div class="info-box">
                            <i class="fa fa-check"></i>
                            <h3>Aspirasi / Pengaduan Anda telah berhasil dikirim! <h3>
                            <p>Anda dapat melakukan penelusuran progress dari laporan Anda dengan memasukkan kode tiket berikut:
                                <br></p>
                            <p style="font-size: 34px; font-weight:bold; color: #ed502e;margin-top:25px;">{{$message["ticket"]}}</p>  <br><br>
                            <a class="cta-btn" href="/">Kembali ke Beranda</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @else
        <section id="cta" class="cta" style="padding: 140px 0 60px 0">
            <div class="container aos-init aos-animate" data-aos="zoom-in">
            <div class="text-center">
                <h3>Layanan Aspirasi dan Pengaduan Online</h3> <br>
                <p>Pengaduan Online rumah Sakit Bhayangkara merupakan sebuah platform yang diperuntukkan kepada Masyarakat Umum / Tenaga Medis / Pegawai Rumah Sakit Bhayangkara Denpasar untuk menyampaikan Aspirasi dan  Pengaduan.</p><br>
                <a class="cta-btn" href="#pengaduan">Buat Laporan Sekarang</a>
            </div>

            </div>
        </section>
        <!-- ======= Contact Section ======= -->
        <section id="pengaduan" class="contact section-bg" style="padding: 120px 0 60px 0">
            <div class="container" data-aos="fade-up">
    
            <div class="section-title offset-1">
                <h2>Pengaduan Online</h2>
                <p style="text-transform: initial;">Rs. Bhayangkara</p>
            </div>
                <div class="row">
                    <div class="col-lg-10 offset-1">
                        <form action="{{ url('/home')}}" method="post" role="form" class="php-email-form" enctype="multipart/form-data">
                            @csrf
                            <div class="complaint-form-box">
                                <div class="select-complaint">Sampaikan Laporan Anda</div>
                                <label for="classification_complaint" class="choose-classification">Pilih Tipe Laporan</label>
                                <div class="btn-group btn-complaint-type" id="classification_complaint" data-toggle="buttons">
                                    <label class="btn btn-default active">
                                    <input type="radio" name="kp_id" value="1" class="sr-only" required=""><span>PENGADUAN</span></label>
                                    <label class="btn btn-default">
                                    <input type="radio" name="kp_id" value="2" class="sr-only" required=""><span>ASPIRASI</span></label>
                                    <label class="btn btn-default">
                                    <input type="radio" name="kp_id" value="3" class="sr-only" required=""><span>PERMINTAAN INFORMASI</span></label>
                                </div>
                                <div class="checkbox checkbox-primary checkbox-circle checkbox-inline text-left" data-toggle="tooltip" data-placement="top" data-title="Nama Anda tidak akan terpublikasi pada laporan" data-original-title="" title="">
                                    <input id="anonim" onclick="Anonim()" class="styled" type="checkbox" name="peng_identitas_pelapor" value="0" style="padding-top:5px; ">
                                    <label for="anonim">
                                        Anonim
                                    </label>
                                </div>
                                <div class="checkbox checkbox-primary checkbox-circle checkbox-inline text-left" data-toggle="tooltip" data-placement="top" data-title="Laporan Anda tidak dapat dilihat oleh publik" data-original-title="" title="">
                                    <input id="Rahasia" class="styled" type="checkbox" name="peng_sifat_pengaduan" value="0">
                                    <label for="Rahasia">
                                        Rahasia
                                    </label>
                                </div>
                            </div>
                            <div id="identitas">
                                <div class="form-row">
                                    <div class="col form-group">
                                    <input type="text" class="form-control" id="name" name="peng_nama" placeholder="Nama Anda" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                    <div class="validate"></div>
                                    </div>
                                    <div class="col form-group">
                                    <input type="email" class="form-control" id="email" name="peng_email" placeholder="Email Anda" data-rule="email" data-msg="Please enter a valid email" />
                                    <div class="validate"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="peng_hp" placeholder="No HP Anda" >
                                    <div class="validate"></div>
                                </div>
                            </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="peng_topik" id="subject" placeholder="Ketik Judul Laporan Anda *" data-rule="minlen:8" data-msg="Please enter at least 8 chars of subject" required/>
                            <div class="validate"></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="peng_isi_pengaduan" rows="5" data-rule="required" data-msg="Ketik Isi Laporan Anda" placeholder="Ketik Isi Laporan Anda"></textarea>
                            <div class="validate"></div>
                        </div>
                        <div class="form-group">
                            <select class="form-control" id="dept_id" name="dept_id" required>
                                <option value="" hidden>-- Departemen Yang Dituju --</option>
                                @foreach ($dept as $item)
                                    <option value="{{ $item->id }}">{{ $item->dept_nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="classification_complaint" class="choose-classification">Pilih File Pendukung</label>
                            <input type="file" style="padding:0px;" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
                            text/plain, application/pdf, image/*" name="peng_file" id="peng_file"/>
                        </div>
                        <div class="text-center"><button type="submit">Kirim</button></div>
                        </form>
                    </div>
                </div>
            
            
                <!--
                <div class="col-lg-6">
                <div class="row">
                    <div class="col-md-12">
                    <div class="info-box">
                        <i class="bx bx-map"></i>
                        <h3>Our Address</h3>
                        <p>A108 Adam Street, New York, NY 535022</p>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="info-box mt-4">
                        <i class="bx bx-envelope"></i>
                        <h3>Email Us</h3>
                        <p>info@example.com<br>contact@example.com</p>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="info-box mt-4">
                        <i class="bx bx-phone-call"></i>
                        <h3>Call Us</h3>
                        <p>+1 5589 55488 55<br>+1 6678 254445 41</p>
                    </div>
                    </div>
                </div>
    
                </div>-->
            </div>
        </section><!-- End Contact Section -->
    @endif
    <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts" style="padding-top: 60px;">
        <div class="container" data-aos="fade-up">
  
          <div class="row no-gutters">
            <div class="col-lg-3 col-sm-6 d-md-flex align-items-md-stretch">
                <div class="count-box">
                  <i class="icofont-live-support"></i>
                  <span data-toggle="counter-up">{{$jumlah_pengaduan}}</span>
                  <p><strong>Jumlah Laporan Sekarang</strong></p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 d-md-flex align-items-md-stretch">
              <div class="count-box">
                <i class="icofont-simple-smile"></i>
                <span data-toggle="counter-up">{{$jumlah_tindaklanjut}}</span>
                <p><strong>Jumlah Pengaduan yang telah ditindaklanjuti</strong></p>
              </div>
            </div>
  
            <div class="col-lg-3 col-sm-6 d-md-flex align-items-md-stretch">
              <div class="count-box">
                <i class="icofont-close-circled"></i>
                <span data-toggle="counter-up">{{$jumlah_menyimpang}}</span>
                <p><strong>Jumlah Pengaduan Menyimpang</strong></p>
              </div>
            </div>
            <div class="col-lg-3 col-sm-6 d-md-flex align-items-md-stretch">
              <div class="count-box">
                <i class="icofont-users-alt-5"></i>
                <span data-toggle="counter-up">{{$jumlah_user}}</span>
                <p><strong>Jumlah Pengguna Aktif</strong></p>
              </div>
            </div>
  
          </div>
  
        </div>
    </section><!-- End Counts Section -->
      
@endsection
@section('js')
    <script>
        function Anonim() {
            // Get the checkbox
            var checkBox = document.getElementById("anonim");
            // Get the output text
            var identitas = document.getElementById("identitas");
        
            // If the checkbox is checked, display the output text
            if (checkBox.checked == true){
                identitas.style.display = "none";
            } else {
                identitas.style.display = "block";
            }
        }
    </script>
    <script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
			// Tambah/Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					$('#modal').modal('hide');
					formExecuted(response);
				});
			});
		});

        function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				location.href ='{{url('/home')}}';
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});
		}
	</script>
@endsection
