@extends('front.layouts.app')
@section('title', 'Cek Pengaduan')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
        <section id="why-us" class="why-us section-bg" style="padding-top:90px !important;">
            <div class="container-fluid" data-aos="fade-up">
      
              <div class="row">
      
                <div class="col-lg-5 align-items-stretch video-box" style='background-image: url("{{ asset('images/rs_trijata_img.jpg') }}");' data-aos="zoom-in" data-aos-delay="100">
                </div>
      
                <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch">
                @if ($data->status==0)
                    <div class="content">
                        <h3>Status Laporan Pengaduan dengan ID Tikcet  <strong>{{ $ticket }}</strong></h3>
                        <p>
                            ID Ticket yang Anda masukkan tidak ditemukan dalam Sistem Kami! Silahkan periksa kembali ID Ticket Laporan Anda.
                        </p>
                    </div>
                @else
                    @php
                        $status = "";
                        if($data->peng_verifikasi=="0"){
                            $status = "Pengaduan Anda menunggu Verifikasi oleh Admin Sistem.";
                        }elseif ($data->peng_verifikasi=="1") {
                            $status = "Pengaduan Anda telah Verifikasi oleh Admin Sistem dan menunggu tindak lanjut.";
                        }elseif ($data->peng_verifikasi=="2") {
                            $status = "Pengaduan Anda dianggap menyimpang oleh Admin Sistem.";
                        }elseif ($data->peng_verifikasi=="3") {
                            $status = "Pengaduan Anda telah Verifikasi dan Ditindak Lanjuti.";
                        }
                    @endphp
                    <div class="content">
                        <h3>Status Laporan Pengaduan dengan ID Tikcet<strong>{{$ticket}}</strong></h3>
                        <p>
                            Terima kasih telah membuat Laporan Pengaduan kepada Kami, guna meningkatkan pelayanan kepada Masyarakat Umum.
                        </p>
                    </div>
        
                    <div class="accordion-list">
                        <ul>
                        <li>
                            <a  class="collapse" href="#"><span>01</span>  {{$status}} </a>
                            <div id="accordion-list-1" class="collapse show" data-parent=".accordion-list">
                            </div>
                        </li>
                        </ul>
                  </div>
                @endif
                  
      
                </div>
      
              </div>
      
            </div>
          </section><!-- End Why Us Section -->
@endsection
@section('js')
    <script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
			// Tambah/Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					$('#modal').modal('hide');
					formExecuted(response);
				});
			});
		});

        function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				location.href ='{{url('/home')}}';
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});
		}
	</script>
@endsection
