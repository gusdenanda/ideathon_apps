@extends('layout.layout')

@section('title', 'Produk')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
        {{-- BEGIN: Table --}}
        <x-datatable-button title="Data Produk" id="tambah" buttonTitle='<i data-feather="plus"></i> Tambah'>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th data-priority="1">Nama Produk</th>
                        <th>Deskripsi</th>
                        <th>Harga</th>
						<th>Stok</th>
						<th>Status</th>
						<th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>

            <form id="delete-form" action="#" method="POST" class="d-none">
                @csrf
                @method('DELETE')
            </form>
        </x-datatable-button>
        {{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="" type="normal" class="" id="modal">
            <form name="form" id="form" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">Toko <span style="color:red">*</span></label>
                        </div>
                        <div class="col-sm-9">
                            <select class="form-control" id="store_id" name="store_id" required>
                                <option value="" hidden>-- Pilih Toko --</option>
                                @foreach ($store as $item)
                                    <option value="{{ $item->id }}">{{ $item->store_nama }}</option>
                                @endforeach
                            </select>
                        </div>
					</div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">Nama Produk <span style="color:red">*</span></label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="produk_nama" id="produk_nama" placeholder="Nama Produk" required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">Kategori Produk <span style="color:red">*</span></label>
                        </div>
                        <div class="col-sm-9">
                            <select class="form-control" id="kp_id" name="kp_id" required>
                                <option value="" hidden>-- Kategori Produk --</option>
                                @foreach ($kp as $item)
                                    <option value="{{ $item->id }}">{{ $item->kp_nama }}</option>
                                @endforeach
                            </select>
                        </div>
					</div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">Harga Produk <span style="color:red">*</span></label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="produk_harga" id="produk_harga" placeholder="Harga Produk" required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">Stok Produk <span style="color:red">*</span></label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="produk_stok" id="produk_stok" placeholder="Stok Produk" required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">Deskripsi </label>
                        </div>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="produk_deskripsi" id="produk_deskripsi" placeholder="Deskripsi Produk" rows="4" ></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">Status <span style="color:red">*</span></label>
                        </div>
                        <div class="col-sm-9">
                            <div>
                                <input type="radio" id="produk_active_1" name="produk_active"  value="1" />
                                <label for="radio_1">Aktif</label>
                            </div>
                    
                            <div>
                                <input type="radio" id="produk_active_0" name="produk_active"  value="0" />
                                <label for="radio_2">Tidak Aktif</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                            <label for="first-name">Gambar Produk </label>
                        </div>
                        <div class="col-sm-9">
                            <input type="file" class="form-control-file" accept="image/*" name="logo" id="logo"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-form-label">
                        </div>
                        <div class="col-sm-9">
                            <div id="filettd"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary mr-1" id="button-tambah"><i data-feather="check"></i> Simpan</button>
                    <button type="submit" class="btn btn-primary mr-1" id="button-ubah"><i data-feather="edit-2"></i> Ubah</button>
                    <button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
                    <button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> Hapus</button>
                </div>
            </form>
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/extensions/ext-component-sweet-alerts.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = null;
	</script>
    {{-- Datatable --}}
	<script>
		var link = "{{ route('admin.master.produk.data') }}";
		var column = [
			{data: 'produk_nama', name: 'produk_nama'},
			{data: 'produk_deskripsi', name: 'produk_deskripsi'},
			{data: 'produk_harga', name: 'produk_harga'},
            {data: 'produk_stok', name: 'produk_stok'},
            {data: 'produk_active', name: 'produk_active', orderable: false, searchable: false},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
	<script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('.flatpickr-basic').flatpickr();
			$('#tambah').on('click', function() {
				$('.modal-title').text('Tambah Toko');
                $('#form').attr('action', "{{ url('/admin/master/produk') }}");
				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);
				
				$('#modal').modal('show');
			});

			$(document).on('click', '.ubah', function() {
				const id = $(this).attr('data-value');
				$('#button-hapus').attr('onClick', "hapus("+ $(this).attr('data-value') +")");
                $('#form').append('<input type="hidden" id="method" name="_method" value="PUT"/>');
				$.get( "{{ url('/admin/master/produk') }}/"+ id, function( data ) {
					var d = JSON.parse(data);
					$('.modal-title').text('Ubah Toko');
					$('#form').attr('action', "{{ url('/admin/master/produk') }}/" + id);
					$('#produk_nama').val(d.produk_nama);
					$('#store_id').val(d.store_id);
					$('#kp_id').val(d.kp_id);
					$('#produk_harga').val(d.produk_harga);
					$('#produk_stok').val(d.produk_stok);
					$('#produk_deskripsi').val(d.produk_deskripsi);
					$('#produk_active_'+ d.produk_active).prop('checked', true);
                    if(d.produk_gambar!=''){
                        $('#filettd').append('<img src="repositories/public_html/images/'+ d.produk_gambar +'" class="img-fluid">');
                    }
				});

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);


				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('#form').attr('action', '');
                $('#method').remove();
				document.getElementById("form").reset();
			});

			// Tambah/Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					$('#modal').modal('hide');
					formExecuted(response);
				});
			});
		});

        function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});
		}

		// Hapus Data
		function hapus(id) {
			Swal.fire({
				title: 'Yakin ingin hapus?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("/admin/master/produk")}}/'+ id,
						type: "POST",
						data: {
							_method: 'DELETE',
						},
					}).done(function(response){
						if (response.id == 1) {
							status = 'success';
							table.ajax.reload();
							$('#modal').modal('hide');
						} else {
							status = 'error';
						}
						toastr[status](response.keterangan, response.status, {
							closeButton: true,
							tapToDismiss: true
						});
					});
				}
			});
		}
	</script>
@endsection