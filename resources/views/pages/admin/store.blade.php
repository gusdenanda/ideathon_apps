@extends('layout.layout')

@section('title', 'Store')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
        {{-- BEGIN: Table --}}
        <x-datatable-button title="Data Store" id="tambah" buttonTitle='<i data-feather="plus"></i> Tambah'>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th data-priority="1">Nama Toko</th>
                        <th>Nama Pemilik</th>
                        <th>Alamat</th>
						<th>Deskripsi</th>
						<th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>

            <form id="delete-form" action="#" method="POST" class="d-none">
                @csrf
                @method('DELETE')
            </form>
        </x-datatable-button>
        {{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="" type="normal" class="" id="modal">
            <form name="form" id="form" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <x-form-group title="Nama Toko">
                        <input type="text" class="form-control" name="store_nama" id="store_nama" placeholder="Nama Toko" required/>
                    </x-form-group>
                    <x-form-group title="Nama Pemilik">
                        <input type="text" class="form-control" name="store_nama_pemilik" id="store_nama_pemilik" placeholder="Nama Pemilik"/>
                    </x-form-group>
                    <x-form-group title="Email">
                        <input type="email" class="form-control" name="store_email" id="store_email" placeholder="Email"/>
                    </x-form-group>
                    <x-form-group title="No HP">
                        <input type="text" class="form-control" name="store_no_hp" id="store_no_hp" placeholder="No HP"/>
                    </x-form-group>
                    <x-form-group title="Deskripsi">
						<textarea class="form-control" id="store_deskripsi" name="store_deskripsi" placeholder="Deskripsi" rows="4"></textarea>
					</x-form-group>
                    <x-form-group title="Alamat Toko">
						<textarea class="form-control" id="store_alamat" name="store_alamat" placeholder="Alamat Toko" rows="4"></textarea>
					</x-form-group>
                    <x-form-group title="Latitude">
                        <input type="text" class="form-control" name="store_lat" id="store_lat" placeholder="Latitude"/>
                    </x-form-group>
                    <x-form-group title="Longitude">
                        <input type="text" class="form-control" name="store_lng" id="store_lng" placeholder="Longitude"/>
                    </x-form-group>
					<x-form-group title="Logo Toko">
						<input type="file" class="form-control-file" accept="image/*" name="logo" id="logo"/>
					</x-form-group>
                    <div id="filettd"></div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary mr-1" id="button-tambah"><i data-feather="check"></i> Simpan</button>
                    <button type="submit" class="btn btn-primary mr-1" id="button-ubah"><i data-feather="edit-2"></i> Ubah</button>
                    <button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
                    <button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> Hapus</button>
                </div>
            </form>
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/extensions/ext-component-sweet-alerts.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = null;
	</script>
    {{-- Datatable --}}
	<script>
		var link = "{{ route('admin.master.store.data') }}";
		var column = [
			{data: 'store_nama', name: 'store_nama'},
			{data: 'store_nama_pemilik', name: 'store_nama_pemilik'},
			{data: 'store_alamat', name: 'store_alamat'},
            {data: 'store_deskripsi', name: 'store_deskripsi'},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
	<script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('.flatpickr-basic').flatpickr();
			$('#tambah').on('click', function() {
				$('.modal-title').text('Tambah Toko');
                $('#form').attr('action', "{{ url('/admin/master/store') }}");
				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);
				
				$('#modal').modal('show');
			});

			$(document).on('click', '.ubah', function() {
				const id = $(this).attr('data-value');
				$('#button-hapus').attr('onClick', "hapus("+ $(this).attr('data-value') +")");
                $('#form').append('<input type="hidden" id="method" name="_method" value="PUT"/>');
				$.get( "{{ url('/admin/master/store') }}/"+ id, function( data ) {
					var d = JSON.parse(data);
					$('.modal-title').text('Ubah Toko');
					$('#form').attr('action', "{{ url('/admin/master/store') }}/" + id);
					$('#store_nama').val(d.store_nama);
					$('#store_email').val(d.store_email);
					$('#store_no_hp').val(d.store_no_hp);
					$('#store_nama_pemilik').val(d.store_nama_pemilik);
					$('#store_deskripsi').val(d.store_deskripsi);
					$('#store_alamat').val(d.store_alamat);
					$('#store_lat').val(d.store_lat);
					$('#store_lng').val(d.store_lng);
					$('#store_logo').val(d.store_logo);
                    if(d.store_logo!=''){
                        $('#filettd').append('<img src="repositories/public_html/images/'+ d.store_logo +'" class="img-fluid">');
                    }
				});

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);


				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('#form').attr('action', '');
                $('#method').remove();
				document.getElementById("form").reset();
			});

			// Tambah/Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					$('#modal').modal('hide');
					formExecuted(response);
				});
			});
		});

        function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});
		}

		// Hapus Data
		function hapus(id) {
			Swal.fire({
				title: 'Yakin ingin hapus?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("/admin/master/store")}}/'+ id,
						type: "POST",
						data: {
							_method: 'DELETE',
						},
					}).done(function(response){
						if (response.id == 1) {
							status = 'success';
							table.ajax.reload();
							$('#modal').modal('hide');
						} else {
							status = 'error';
						}
						toastr[status](response.keterangan, response.status, {
							closeButton: true,
							tapToDismiss: true
						});
					});
				}
			});
		}
	</script>
@endsection