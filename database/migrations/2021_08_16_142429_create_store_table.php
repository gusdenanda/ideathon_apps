<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store', function (Blueprint $table) {
            $table->increments("id");
            $table->string("store_nama");
            $table->string("store_deskripsi");
            $table->text("store_alamat");
            $table->string("store_email");
            $table->string("store_no_hp");
            $table->string("store_nama_pemilik");
            $table->string("store_logo");
            $table->double("store_lat");
            $table->double("store_lng");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store');
    }
}
