<?php

use App\Models\ListRole;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
require __DIR__.'/auth.php';

Route::get('', 'HomeController@index');
Route::get('/', 'HomeController@index');

Route::namespace('Admin')->name('admin.')->prefix('admin')->middleware('auth', 'role:admin')->group(function() {
    Route::namespace('Master')->name('master.')->prefix('master')->group(function() {
        Route::get('', 'HomeController@index');
        Route::resource('user', 'UserController');
        Route::get('data/user', 'UserController@data')->name('user.data');

        Route::resource('store', 'StoreController');
        Route::get('data/store', 'StoreController@data')->name('store.data');

        Route::resource('kategori-produk', 'KategoriProdukController');
        Route::get('data/kategori-produk', 'KategoriProdukController@data')->name('kategori-produk.data');

        Route::resource('produk', 'ProdukController');
        Route::get('data/produk', 'ProdukController@data')->name('produk.data');
    });
});
